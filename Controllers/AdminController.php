<?php

require_once 'AppController.php';
require_once __DIR__.'/../Models/User.php';
require_once __DIR__.'/../Repository/UserRepository.php';

class AdminController extends AppController {

    public function index(): void 
    {
        $userRepository = new UserRepository();
        if(isset($_SESSION['id'])) {
            $this->render('index', ['user' => $userRepository->getUser($_SESSION['id'])]);
        } else echo "You are not logged in!";
    }


    public function users(): void
    {   
        $userRepository = new UserRepository();
        
        header('Content-type: application/json');
        http_response_code(200);
        
        echo $userRepository->getUsers() ? json_encode($userRepository->getUsers()) : '';
    }

    public function userDelete(): void
    {
        if (!isset($_POST['id'])) {
            http_response_code(404);
            return;
        }

        $userRepository = new UserRepository();
        $userRepository->delete((int)$_POST['id']);

        http_response_code(200);
    }
}