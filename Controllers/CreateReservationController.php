<?php

require_once 'AppController.php';
require_once __DIR__.'/../Models/Service.php';
require_once __DIR__.'/../Repository/ServiceRepository.php';
require_once __DIR__.'/../Models/Building.php';
require_once __DIR__.'/../Repository/BuildingRepository.php';
require_once __DIR__.'/../Repository/ReservationRepository.php';

class CreateReservationController extends AppController {

    public function buildings(): void
    {   
        $buildingRepository = new BuildingRepository();

        header('Content-type: application/json');
        http_response_code(200);

        echo $buildingRepository->getBuildings() ? json_encode($buildingRepository->getBuildings()) : '';
    }

    public function services()
    {   
        $serviceRepository = new ServiceRepository();

        header('Content-type: application/json');
        http_response_code(200);

        echo $serviceRepository->getServicesJSON() ? json_encode($serviceRepository->getServicesJSON()) : '';
    }

    public function getServices()
    {   
        $serviceRepository = new ServiceRepository();
        $services = $serviceRepository->getServices();

        $this->render('create_reservation', ['create_reservation' => $services]);
    }

    public function makeReservation() 
    {
        $reservationRepository = new ReservationRepository();


        $user_id = $_SESSION['id'];
        $service_id = $_POST['services'];
        $date = $_POST['dates'];
        $start_time = $_POST['start_time'];
        $end_time = $_POST['end_time'];
        
        if ($reservationRepository->validateReservation($user_id, $service_id, $date, $start_time, $end_time)) 
        {
            $reservationRepository->makeReservation($user_id, $service_id, $date, $start_time, $end_time);
        }

        
        return;
    }
}
