<?php

require_once 'AppController.php';
require_once __DIR__.'/../Models/Reservation.php';
require_once __DIR__.'/../Repository/ReservationRepository.php';

class MyReservationsController extends AppController {

    public function getReservations()
    {   
        $reservationRepository = new ReservationRepository();
        $reservations = $reservationRepository->getUserReservations($_SESSION['id']);

        $this->render('my_reservations', ['reservations' => $reservations]);
    }
}