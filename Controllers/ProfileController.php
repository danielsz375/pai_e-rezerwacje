<?php

require_once 'AppController.php';
require_once __DIR__.'/../Models/User.php';
require_once __DIR__.'/../Repository/UserRepository.php';

class ProfileController extends AppController {

    public function getUserData()
    {   
        $userRepository = new UserRepository();
        $user = $userRepository->getUser($_SESSION['id']);

        $this->render('profile', ['user' => $user]);
    }
}