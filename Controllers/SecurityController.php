<?php

require_once 'AppController.php';
require_once __DIR__.'/../Models/User.php';
require_once __DIR__.'/../Repository/UserRepository.php';

class SecurityController extends AppController {

    public function login()
    {   
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $id = $_POST['id'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($id);

            if (!$user) {
                $this->render('login', ['messages' => ['Użytkownik o podanym loginie nie istnieje!']]);
                return;
            }

            if ($user->getPassword() !== $password) {
                $this->render('login', ['messages' => ['Złe hasło!']]);
                return;
            }

            $_SESSION["role"] = $user->getRole();
            $_SESSION["email"] = $user->getEmail();
            $_SESSION["id"] = $user->getId();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/pai2019/?page=my_reservations");
            return;
        }

        $this->render('login');
    }

    public function logout()
    {
        session_unset();
        session_destroy();

        $this->render('login', ['messages' => ['Zostałeś pomyślnie wylogowany!']]);
    }
}