<?php

require_once 'AppController.php';
require_once __DIR__.'/../Models/Service.php';
require_once __DIR__.'/../Repository/ServiceRepository.php';

class ServicesController extends AppController {

    public function getServices()
    {   
        $serviceRepository = new ServiceRepository();
        $services = $serviceRepository->getServices();

        $this->render('services', ['services' => $services]);
    }
}