<?php

class Reservation {
    private $id;
    private $serviceName;
    private $buildingName;
    private $date;
    private $startHour;
    private $endHour;
    private $totalPrice;
    private $status;

    public function __construct(
        int $id,
        string $serviceName,
        string $buildingName,
        string $date,
        string $startHour,
        string $endHour,
        float $totalPrice,
        int $status
    ) {
        $this->id = $id;
        $this->serviceName = $serviceName;
        $this->buildingName = $buildingName;
        $this->date = $date;
        $this->startHour = $startHour;
        $this->endHour = $endHour;
        $this->totalPrice = $totalPrice;
        $this->status = $status;
    }


    public function getId(): int {
		return $this->id;
	}

	public function getServiceName(): string {
		return $this->serviceName;
	}

	public function getBuildingName(): string {
		return $this->buildingName;
	}

	public function getDate(): string {
		return $this->date;
	}

	public function getStartHour(): string {
		return $this->startHour;
	}

	public function getEndHour(): string {
		return $this->endHour;
	}

	public function getTotalPrice(): float {
		return $this->totalPrice;
	}

	public function getStatus(): int {
		return $this->status;
	}
}


