<?php

class Service {
    private $id;
    private $name;
    private $price_per_hour;
    private $building_id;
    private $working_hours_id;

    public function __construct(
        int $id,
        string $name,
        string $price_per_hour,
        int $building_id,
        int $working_hours_id
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->price_per_hour = $price_per_hour;
        $this->building_id = $building_id;
        $this->working_hours_id = $working_hours_id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice_per_hour(): string
    {
        return $this->price_per_hour;
    }


    public function getBuilding_id(): int
    {
        return $this->building_id;
    }

    public function getWorking_hours_id(): int
    {
        return $this->working_hours_id;
    }
}