
function getBuildings() {
    const apiUrl = "http://localhost/pai2019";
    const $list = $('.buildings');
    $.ajax({
        url : apiUrl + '/?page=buildings_all',
        dataType : 'json'
    })
        .done((res) => {
            res.forEach(el => {
                $list.append(`
                <option value="${el.id}">
                    ${el.name}
                </option>`);
            })
        });
    resetTimeInputs();
}

function getServices($building_id) {
    const apiUrl = "http://localhost/pai2019";
    const $list = $('.services');
    $list.empty();
    $list.append(`
            <option value="" selected disabled hidden>
                Wybierz usługę
            </option>
        `);
    $.ajax({
        url : apiUrl + '/?page=services_all',
        dataType : 'json',
    })
        .done((res) => {
            res.forEach(el => {
                if($building_id == el.building_id) {
                        $list.append(`
                            <option value="${el.id}">
                                ${el.name}
                            </option>
                        `);
                    }
                
            })
        });
    resetTimeInputs();
}

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function getDayName(day) {
    if(day >= 0 && day < 7) {
        var days = ["Niedziela","Poniedziałek","Wtorek","Środa","Czwartek","Piątek","Sobota"];
        return days[day];
    }
}

function romanize(num) {
    var lookup = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},roman = '',i;
    for ( i in lookup ) {
      while ( num >= lookup[i] ) {
        roman += i;
        num -= lookup[i];
      }
    }
    return roman;
  }

function getDates($service_id) {
    const apiUrl = "http://localhost/pai2019";
    const $list = $('.dates');
    $list.empty();
    $list.append(`
            <option value="" selected disabled hidden>
                Wybierz termin rezerwacji
            </option>
        `);
    $.ajax({
        url : apiUrl + '/?page=services_all',
        dataType : 'json',
    })
        .done((res) => { 
            var d = new Date(); 
            var today = d.getDay();
            var dayOfWeek;
            var newDate;
            res.forEach(el => {
                if($service_id == el.id) {
                    for(var i = 0; i < 7; i++) {
                        newDate = addDays(d, i);
                        dayOfWeek = newDate.getDay();
                        if((dayOfWeek >= 1 && dayOfWeek <= 5) && el.start_time != el.end_time) {
                            $list.append(`
                                <option value="${newDate.getTime()/1000}">
                                    ${newDate.getDate()} ${romanize(newDate.getMonth() + 1)} ${newDate.getFullYear()} (${getDayName(newDate.getDay())})
                                </option>
                            `);
                        } else if ((dayOfWeek == 0 || dayOfWeek == 6) && el.weekend_start_time != el.weekend_end_time) {
                            $list.append(`
                                <option value="${newDate.getTime()/1000}">
                                    ${newDate.getDate()} ${romanize(newDate.getMonth() + 1)} ${newDate.getFullYear()} (${getDayName(newDate.getDay())})
                                </option>
                            `);
                        }
                    }
                    
                        
                }
                
            })
        });
    resetTimeInputs();      
}


function resetDatesList() {
    $('#dates').val("");
    $('#dates').empty();
    $('#dates').append(`
        <option value="" selected disabled hidden>
            Wybierz termin rezerwacji
        </option>
    `);
}

function getHours($time) {
    return parseInt($time.substring(0, 2));
}

function getMinutes($time) {
    return parseInt($time.substring(3, 5));
}

function isTimeBetween($givenTime, $startTime, $endTime) {
    var hours = getHours($givenTime);
    var minutes = getMinutes($givenTime);

    var hoursStartTime = getHours($startTime);
    var minutesStartTime = getMinutes($startTime);

    var hoursEndTime = getHours($endTime);
    var minutesEndTime = getMinutes($endTime);

    if(hours >= hoursStartTime && hours <= hoursEndTime) {
        return true;
    } else if (hours == hoursStartTime && minutes >= minutesStartTime) {
        return true;
    } else if (hours == hoursEndTime && minutes <= minutesEndTime) {
        return true;
    } else {
        return false;
    }
}

function validateStartTime($service_id, $dateTimestamp, $start_time) {
    const apiUrl = "http://localhost/pai2019";
    $.ajax({
        url : apiUrl + '/?page=services_all',
        dataType : 'json',
    })
            .done((res) => { 
                res.forEach(el => {
                    if($service_id == el.id) {
                        var date = new Date($dateTimestamp * 1000);
                        var dayOfWeek = date.getDay();
                        if((dayOfWeek >= 1 && dayOfWeek <= 5) && el.start_time != el.end_time) {

                            if(isTimeBetween($start_time, el.start_time, el.end_time)) {
                                document.getElementById("start_time").style.borderColor="#0fbe0f";
                            } else {
                                document.getElementById("start_time").style.borderColor="#e61414";
                            }

                        } else if ((dayOfWeek == 0 || dayOfWeek == 6) && el.weekend_start_time != el.weekend_end_time) {
                            
                            if(isTimeBetween($start_time, el.weekend_start_time, el.weekend_end_time)) {
                                document.getElementById("start_time").style.borderColor="#0fbe0f";
                            } else {
                                document.getElementById("start_time").style.borderColor="#e61414";
                            }

                        }
                        calculatePrice(parseFloat(el.price_per_hour));
                    }
                })
            });
    
    validateEndTime(
        document.getElementById("services").value,
        document.getElementById("dates").value,
        document.getElementById("start_time").value,
        document.getElementById("end_time").value
    )
}



function validateEndTime($service_id, $dateTimestamp, $start_time, $end_time) {
    const apiUrl = "http://localhost/pai2019";
    $.ajax({
        url : apiUrl + '/?page=services_all',
        dataType : 'json',
    })
            .done((res) => { 
                res.forEach(el => {
                    if($service_id == el.id) {
                        var date = new Date($dateTimestamp * 1000);
                        var dayOfWeek = date.getDay();

                        if((dayOfWeek >= 1 && dayOfWeek <= 5) && el.start_time != el.end_time) {
                            if(isTimeBetween($end_time, $start_time, el.end_time) 
                                && isTimeBetween($end_time, el.start_time, el.end_time)) {
                                document.getElementById("end_time").style.borderColor="#0fbe0f";
                            } else {
                                document.getElementById("end_time").style.borderColor="#e61414";
                            }

                        } else if ((dayOfWeek == 0 || dayOfWeek == 6) && el.weekend_start_time != el.weekend_end_time) {
                            
                            if(isTimeBetween($end_time, $start_time, el.weekend_end_time)
                                && isTimeBetween($end_time, el.weekend_start_time, el.end_time)) {
                                document.getElementById("end_time").style.borderColor="#0fbe0f";
                            } else {
                                document.getElementById("end_time").style.borderColor="#e61414";
                            }

                        }
                        calculatePrice(parseFloat(el.price_per_hour));
                    }
                })
            });
}

function resetInput($inputName) {
    $('input[id=' + $inputName).val('');
    $('input[id=' + $inputName).css({'border-color': "#bce0fd"});
}

function resetTimeInputs() {
    resetInput("start_time");
    resetInput("end_time");
    resetPrice();
}

function showTimeInputs(that) {
    if (that.value != "") {
        document.getElementById("start_time").style.display = "block";
        document.getElementById("end_time").style.display = "block";
    } else {
        hideTimeInputs();
    }
    resetTimeInputs(); 
}

function hideTimeInputs() {
    document.getElementById("start_time").style.display = "none";
    document.getElementById("end_time").style.display = "none";
}

function resetPrice() {
    $('#price').val("Cena");
    $('#price').prop('readonly', true);
    $("#price").css({'border-color': "#bce0fd"});
    disableSubmitButton();
}

function enableSubmitButton() {
    $("#reserve").removeAttr("disabled");  
}

function disableSubmitButton() {
    $('#reserve').attr("disabled", "disabled");
}

function validatePrice() {
    if( areStringsEqual(document.getElementById("start_time").style.borderColor, 'rgb(15, 190, 15)')
        && areStringsEqual(document.getElementById("end_time").style.borderColor, 'rgb(15, 190, 15)')
        && areStringsEqual(document.getElementById("price").style.borderColor, 'rgb(15, 190, 15)') ) {
            enableSubmitButton();
    } else {
        disableSubmitButton();
    }
}

function validateReserveButton() {
    alert("validate");
    if( $("#start_time").css('border-color') == '#0fbe0f'
        && $("#end_time").css('border-color') == '#0fbe0f' 
        && $("#price").css('border-color') == '#0fbe0f' ) {
            enableSubmitButton();
    } else {
        disableSubmitButton();
    }
}

function calculatePrice($price_per_hour) {
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();
    var start_date = new Date(2000, 0, 1, getHours(start_time), getMinutes(start_time));
    var end_date = new Date(2000, 0, 1, getHours(end_time), getMinutes(end_time));
    if(end_date > start_date
        && areStringsEqual(document.getElementById("start_time").style.borderColor, 'rgb(15, 190, 15)')
        && areStringsEqual(document.getElementById("end_time").style.borderColor, 'rgb(15, 190, 15)')) {
        var difference = end_date - start_date;
        var difference_hours = (difference / 1000 / 60 / 60);
        var total_cost = ($price_per_hour * difference_hours).toFixed(2);
        $('#price').val(total_cost + " PLN");
        $('#price').css({'border-color': "#0fbe0f"});
    } else {
        $('#price').val("Niewłaściwy zakres godzin.");
        $('#price').css({'border-color': "#e61414"});
    }
    validatePrice();
    
}

function findDiff(str1, str2){ 
    let diff = 0;
    str2.split('').forEach(function(val, i){
      if (val != str1.charAt(i))
        diff += 1;         
    });
    return diff;
  }

function areStringsEqual(str1, str2) {
    if(findDiff(str1, str2) == 0) {
        return true;
    } else return false;
}


$( document ).ready(function() {
    var headerName = $(".content-header h2").html();
    $(".topnav-header h2").html(headerName);
});