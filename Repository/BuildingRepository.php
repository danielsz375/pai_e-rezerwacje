<?php

require_once "Repository.php";
require_once __DIR__.'/../Models/Building.php';

class BuildingRepository extends Repository {


    public function getBuildings() {
        try {
            $stmt = $this->database->connect()->prepare('
            SELECT * FROM building;
            ');
            $stmt->execute();

            $buildings = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            return $buildings;
        } catch(PDOException $e) {
            die();
        }
    }

}