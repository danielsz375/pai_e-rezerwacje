<?php

require_once "Repository.php";
require_once __DIR__.'/../Models/Reservation.php';

class ReservationRepository extends Repository {


    public function getUserReservations($user_id) 
    {
        try {
            $stmt = $this->database->connect()->prepare('
            SELECT * FROM reservation_full WHERE user_id = :user_id;
            ');
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);
            $stmt->execute();

            $reservations_array = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $user_reservations = [];
            foreach($reservations_array as $reservation)
            {
                $newReservation = new Reservation(
                    $reservation["id"],
                    $reservation["service_name"],
                    $reservation["building_name"],
                    $reservation["date"],
                    $reservation["start_time"],
                    $reservation["end_time"],
                    $reservation["total_price"],
                    $reservation["is_paid"]
                );
                array_push($user_reservations, $newReservation);
            }

            return $user_reservations;
        } catch(PDOException $e) {
            die();
        }
    }

    public function makeReservation(int $user_id, int $service_id, string $date, string $start_time, string $end_time): void 
    {
        try {
            $myPDO = $this->database->connect();
            $myPDO->beginTransaction();

            $stmt = $myPDO->prepare('
            SELECT price_per_hour FROM service_full WHERE id = :service_id;
                ');
            $stmt->bindParam(':service_id', $service_id, PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $price_per_hour = $res[0]['price_per_hour'];

        
            $timestamp_difference = strtotime($end_time) - strtotime($start_time);
            $hours_difference = $timestamp_difference / 60 / 60;
            $total_price = $hours_difference * $price_per_hour;
            $rounded_total_price = round($total_price, 2);

            $stmt = $myPDO->prepare('
            INSERT INTO reservation(user_id, service_id, date, start_time, end_time, total_price) VALUES (?, ?, ?, ?, ?, ?) 
            ');
            $converted_date = date('Y-m-d', $date);


            $stmt->bindParam(1, $user_id);
            $stmt->bindParam(2, $service_id);
            $stmt->bindParam(3, $converted_date);
            $stmt->bindParam(4, $start_time);
            $stmt->bindParam(5, $end_time);
            $stmt->bindParam(6, $rounded_total_price);

            $stmt->execute();

            $myPDO->commit();
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/pai2019/?page=my_reservations");
        } catch(PDOException $e) {
            if ($myPDO->inTransaction()) {
                $myPDO->rollback();
            }
            die();
        }
    }

    public function validateReservation($user_id, $service_id, $date, $start_time, $end_time)
    {
        try {
            $stmt = $this->database->connect()->prepare('
            SELECT COUNT(*) FROM user WHERE id = :user_id;
            ');
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = $this->database->connect()->prepare('
            SELECT COUNT(*) FROM service WHERE id = :service_id;
            ');
            $stmt->bindParam(':service_id', $service_id, PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $dayOfWeek = date('w', $date);

            if($dayOfWeek == 0 || $dayOfWeek == 6) 
            {
                $stmt = $this->database->connect()->prepare('
                SELECT weekend_start_time, weekend_end_time FROM service_full WHERE id = :service_id;
                ');
                $stmt->bindParam(':service_id', $service_id, PDO::PARAM_STR);
                $stmt->execute();
                $time_limits = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if(strtotime($start_time) >= strtotime($time_limits[0]['weekend_start_time'])) {
                    if(strtotime($end_time) <= strtotime($time_limits[0]['weekend_end_time'])) {
                        return true;
                    } 
                } 
                

            } elseif ($dayOfWeek >= 1 && $dayOfWeek <= 5) {
                $stmt = $this->database->connect()->prepare('
                SELECT start_time, end_time FROM service_full WHERE id = :service_id;
                ');
                $stmt->bindParam(':service_id', $service_id, PDO::PARAM_STR);
                $stmt->execute();
                $time_limits = $stmt->fetchAll(PDO::FETCH_ASSOC);
                if(strtotime($start_time) >= strtotime($time_limits[0]['start_time'])) {
                    if(strtotime($end_time) <= strtotime($time_limits[0]['end_time'])) {
                        return true;
                    }
                } 
            } 
            return false;
        } catch(PDOException $e) {
            die();
        }
        
    }


}