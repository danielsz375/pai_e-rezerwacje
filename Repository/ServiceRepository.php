<?php

require_once "Repository.php";
require_once __DIR__.'/../Models/Service.php';

class ServiceRepository extends Repository {

    public function getService(int $id): Service 
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM service WHERE id = :id;
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        $service = $stmt->fetch(PDO::FETCH_ASSOC);

        if($service == false) {
            return null;
        }

        return new Service(
            $service['id'],
            $service['name'],
            $service['price_per_hour'],
            $service['building_id'],
            $service['working_hours_id']
        );
    }

    public function getServices(): array {
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM service;
        ');
        $stmt->execute();
        $services = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($services as $service) {
            $result[] = new Service(
                $service['id'],
                $service['name'],
                $service['price_per_hour'],
                $service['building_id'],
                $service['working_hours_id']
            );
        }

        return $result;
    }

    public function getServicesJSON() {
        try {
            $stmt = $this->database->connect()->prepare('
            SELECT * FROM service_full;
            ');
            
            $stmt->execute();
            $services = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $services;
        } catch(PDOException $e) {
            die();
        }
    }
}