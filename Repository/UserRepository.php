<?php

require_once "Repository.php";
require_once __DIR__.'/../Models/User.php';

class UserRepository extends Repository {

    public function getUser(int $id)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM user WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        $role = UserRepository::isAdministrator($id) ? ['ROLE_USER', 'ROLE_ADMIN'] : ['ROLE_USER'];

        return new User(
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['id'],
            $role
        );
    }

    public function getUsers() {
        try {
            $stmt = $this->database->connect()->prepare('
            SELECT * FROM user WHERE id != :id;
            ');
            $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
            $stmt->execute();

            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);


            foreach($users as $key => $value) 
            {
                $users[$key]['role'] = UserRepository::isAdministrator( $users[$key]['id']) ? ['ROLE_ADMIN'] : ['ROLE_USER'];
            }

            return $users;
            
        } catch(PDOException $e) {
            die();
        }
    }

    public function delete(int $id): void {
        try {
            $stmt = $this->database->connect()->prepare('
            DELETE FROM user WHERE id = :id;
            ');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
        } catch(PDOException $e) {
            die();
        }
    }


    private function isAdministrator(int $id): bool {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM administrator WHERE user_id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {

            return false;
        }
        
        return true;

    }
}