<?php

require_once 'Controllers/SecurityController.php';
require_once 'Controllers/AdminController.php';
require_once 'Controllers/ProfileController.php';
require_once 'Controllers/ServicesController.php';
require_once 'Controllers/MyReservationsController.php';
require_once 'Controllers/CreateReservationController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'users' => [
                'controller' => 'AdminController',
                'action' => 'users'
            ],
            'profile' => [
                'controller' => 'ProfileController',
                'action' => 'getUserData'
            ],
            'services' => [
                'controller' => 'ServicesController',
                'action' => 'getServices'
            ], 
            'my_reservations' => [
                'controller' => 'MyReservationsController',
                'action' => 'getReservations'
            ],
            'create_reservation' => [
                'controller' => 'CreateReservationController',
                'action' => 'getServices'
            ],
            'admin' => [
                'controller' => 'AdminController',
                'action' => 'index'
            ],
            'admin_users' => [
                'controller' => 'AdminController',
                'action' => 'users'
            ],
            'admin_delete_user' => [
                'controller' => 'AdminController',
                'action' => 'userDelete'
            ], 
            'buildings_all' => [
                'controller' => 'CreateReservationController',
                'action' => 'buildings'
            ],
            'services_all' => [
                'controller' => 'CreateReservationController',
                'action' => 'services'
            ],
            'makeReservation' => [
                'controller' => 'CreateReservationController',
                'action' => 'makeReservation'
            ] 
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'login';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}