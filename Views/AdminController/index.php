<?php include(dirname(__DIR__).'/Common/header.php'); ?>
<?php include(dirname(__DIR__).'/Common/navbar.php'); ?>
<?php
    if(!in_array('ROLE_ADMIN', $_SESSION['role'])) {
        die('You do not have permission to watch this page!');
    }
?>
<script type="text/javascript" src="Public/js/userScript.js"></script>
    <div class="container">
        <div class="content">
            <div class="content-header">
                <h2>Lista użytkowników</h2>
            </div>
            
            <div class="actual-content">
                <div class="table-container">
                    <table id="mytable" class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Email</th>
                            <th scope="col">Imię</th>
                            <th scope="col">Nazwisko</th>
                            <th scope="col">Uprawnienia</th>
                            <th scope="col">Akcja</th>
                            </tr>
                        </thead>
                        <tbody class="logged-admin">
                                <tr>
                                <td><?= $user->getId(); ?></td>
                                <td><?= $user->getEmail(); ?></td>
                                <td><?= $user->getName(); ?></td>
                                <td><?= $user->getSurname(); ?></td>
                                <td><?= in_array('ROLE_ADMIN', $user->getRole()) ? 'ROLE_ADMIN' : 'ROLE_USER'; ?></td>
                                <td>-</td>
                                </tr>
                        </tbody>
                        <tbody id="users-list" class="users-list">
                                
                        </tbody>
                    </table>
                    <button class="btn btn-dark btn-lg" type="button" onclick='this.style.display = "none"; getUsers()'>Pobierz wszystkich użytkowników</button>
                </div>
            </div>
            
        </div>  
    </div>
</body>
</html>
