<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="100">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/adminPage.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/content.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/contentForm.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/createReservation.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/myTable.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/services.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/serviceDetails.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/topNav.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/profile.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/mobile/content.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/mobile/services.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/mobile/topNav.css" />
    <link rel="stylesheet" type="text/css" href="Public/css/mobile/serviceDetails.css" />
    
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>E-rezerwacje</title>
</head>
<body>

