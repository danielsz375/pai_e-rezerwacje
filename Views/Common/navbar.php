<?php
    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        die('You are not logged in!');
    }

    if(!in_array('ROLE_USER', $_SESSION['role'])) {
        die('You do not have permission to watch this page!');
    }
?>
<script type="text/javascript" src="Public/js/dropDownMenu.js"></script>
<div class="topnav" id="myTopNav">
  <div class="topnav-header">
  <h2></h2>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
    </a>
  </div>
  <div id="myLinks">
  <a href="?page=my_reservations">MOJE REZERWACJE</a>
  <a href="?page=services">LISTA USŁUG</a>
  <a href="?page=profile">MÓJ PROFIL</a>
  <?php if(in_array('ROLE_ADMIN', $_SESSION['role'])): ?>
    <a href="?page=logout" style="float: right;">
      WYLOGUJ
    </a>
    <a href="?page=admin" style="float: right;">PANEL ADMINISTRATORA</a>
  <?php else: ?>
    <a href="?page=logout" style="float: right;">
      WYLOGUJ
    </a>
  <?php endif; ?>
  </div>
  
</div>


