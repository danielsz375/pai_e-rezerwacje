<?php include(dirname(__DIR__).'/Common/header.php'); ?>
<?php include(dirname(__DIR__).'/Common/navbar.php'); ?>
<script type="text/javascript" src="Public/js/reservationScript.js"></script>
<div class="container">
    <div class="content">
        <div class="content-header">
            <h2>Utwórz rezerwację</h2>
        </div>
        
        <div class="actual-content">
          <form class="cr" action="?page=makeReservation" method="POST">
      


          <div class="select">
            <select id="buildings" name="buildings" class="buildings" default="Budynek" 
            onchange='getServices(document.getElementById("buildings").value);
                      getDates(document.getElementById("services").value);
                      hideTimeInputs()'
            >
              <option value="" selected disabled hidden>Wybierz budynek</option>
            </select> 
            <div class="select_arrow">
            </div>
          </div>

          <div class="select">
              <select id="services" name="services" class="services" 
              onchange='getDates(document.getElementById("services").value);
                        hideTimeInputs()'>
                <option value="" selected disabled hidden>Wybierz usługę</option>
              </select> 
              <div class="select_arrow">
              </div>
          </div>

          <div class="select">
              <select id="dates" name="dates" class="dates" onchange="showTimeInputs(this);">
                <option value="" selected disabled hidden>Wybierz termin rezerwacji</option>
              </select> 
              <div class="select_arrow">
              </div>
          </div>


          <div class="inputs-container">
            <input id="start_time" name="start_time" type="time" style="display: none;" onblur='validateStartTime(
              document.getElementById("services").value,
              document.getElementById("dates").value,
              document.getElementById("start_time").value
            );'>

            <div style="width: 8%"></div> 
            <input id="end_time" name="end_time"  type="time" style="display: none;" onblur='validateEndTime(
              document.getElementById("services").value,
              document.getElementById("dates").value,
              document.getElementById("start_time").value,
              document.getElementById("end_time").value
            );'>
          </div>
          
          <input id="price" type="text" placeholder="Cena" readonly>
          
          <button id="reserve" type="submit" disabled>ZAREZERWUJ</button>

          </form>
        </div>
    </div>
    
</div>

<script type="text/javascript">
window.onload = function() {
  getBuildings();
};
</script>
</body>
</html>