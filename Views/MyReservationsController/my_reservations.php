<?php include(dirname(__DIR__).'/Common/header.php'); ?>
<?php include(dirname(__DIR__).'/Common/navbar.php'); ?>
<div class="container">
    <div class="content">
        <div class="content-header">
            <h2>Rezerwacje</h2>
        </div>
        <div class="actual-content" style="">
            <div class="table-container">
                <table id="mytable" class="table table-hover" >
                        <thead>
                            <tr>
                            <th scope="col">Nazwa usługi</th>
                            <th scope="col">Nazwa budynku</th>
                            <th scope="col">Data rezerwacji</th>
                            <th scope="col">Godzina rozpoczęcia</th>
                            <th scope="col">Godzina zakończenia</th>
                            <th scope="col">Kwota płatności</th>
                            <th scope="col">Status rezerwacji</th>
                            </tr>
                        </thead>
                        <tbody class="users-list">
                                <?php foreach($reservations as $reservation): ?>
                                        
                                        <tr>
                                        <td scope="row"><?= $reservation->getServiceName(); ?></td>
                                        <td><?= $reservation->getBuildingName(); ?></td>
                                        <td><?= $reservation->getDate(); ?></td>
                                        <td><?= $reservation->getStartHour(); ?></td>
                                        <td><?= $reservation->getEndHour(); ?></td>
                                        <td><?= $reservation->getTotalPrice(); ?> PLN</td>
                                        <td><?= $reservation->getStatus() == 1 ? 'Opłacona' : 'Nieopłacona'; ?></td>
                                        </tr>
                                <?php endforeach ?>
                        </tbody>
                </table>
            </div>
        </div>
    </div>  
</div>
</body>
</html>