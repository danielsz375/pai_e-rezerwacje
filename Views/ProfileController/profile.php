<?php include(dirname(__DIR__).'/Common/header.php'); ?>
<?php include(dirname(__DIR__).'/Common/navbar.php'); ?>
<div class="container">
    <div class="content">
        <div class="content-header">
            <h2>Profil użytkownika</h2>
        </div>
        
        <div class="actual-content">
            <table class="profile-table">
                    <tr>
                        <th>Login:</th>
                        <td><?= $user->getId(); ?></td>
                    </tr>
                    <tr>
                        <th>Imię:</th>
                        <td><?= $user->getName(); ?></td>
                    </tr>
                    <tr>
                        <th>Nazwisko:</th>
                        <td><?= $user->getSurname(); ?></td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td><?= $user->getEmail(); ?></td>
                    </tr>
            </table>
            <h5><a href="#">Zmiana hasła</a></h5>

        </div>
    </div>
    
</div>
</body>
</html>