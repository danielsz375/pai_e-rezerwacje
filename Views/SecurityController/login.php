<?php include(dirname(__DIR__).'/Common/header.php'); ?>
<div class="container">
    <div class="content">
        <div class="content-header">
            <h2 class="login-header">Logowanie</h2>
        </div>
        <div class="actual-content">
            <form action="?page=login" method="POST">
                <h3>Witamy w e-serwisie osiedla studenckiego.</h3>

                    <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                    ?>

                <div class="input-container"> 
                    <i class="fa fa-user icon"> </i> 
                    <input name="id" type="text" placeholder="Login">
                </div> 
                <div class="input-container"> 
                    <i class="fa fa-lock icon"> </i> 
                    <input name="password" type="password" placeholder="Hasło">
                </div> 
                
                <button type="submit">ZALOGUJ</button>
                <h5><a href="#">Zapomniałem hasła</a></h5>
            </form>
        </div>
    </div>
    
</div>
</body>
</html>