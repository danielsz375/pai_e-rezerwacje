<?php $SERVICE_ID = htmlspecialchars($_GET["id"]); ?>
<h2>Szczegóły</h2>
<div class="flex-container">
    <?php foreach($services as $service): ?>
        <?php if($service->getId() == $SERVICE_ID): ?>
            <div class="details-image"></div>
            <div class="details">

            <table class="details-table" style="float: left; margin-top: 0;">
                <tr>
                    <th>Nazwa usługi:</th>
                    <td><?php echo $service->getName(); ?></td>
                </tr>
                <tr>
                    <th>Lokalizacja:</th>
                    <td><?php echo $service->getBuilding_Id(); ?></td>
                </tr>
                <tr>
                    <th>Dostępność usługi:</th>
                    <td><?php echo $service->getWorking_hours_id(); ?></td>
                </tr>
                <tr>
                    <th>Cena:</th>
                    <td><?php echo $service->getPrice_per_hour(); ?> PLN/h</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <form action="?page=create_reservation" method="POST">
                            <button id="details-button" value="<?php $service->getId();?>" type="submit">Zarezerwuj</button>
                        </form> 
                    </td>
                </tr>
            </table>

            

            </div>
            <?php break; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</div>