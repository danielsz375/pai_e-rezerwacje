<?php include(dirname(__DIR__).'/Common/header.php'); ?>
<?php include(dirname(__DIR__).'/Common/navbar.php'); ?>


<div class="container">
    <div class="content">
        
        <?php if(!isset($_GET["id"])): ?>
            <div class="content-header">
                <h2>Lista usług</h2>
            </div>
            <div class="actual-content">

                <div class="section group"> 
                    <?php 
                    $i = 0; 
                    foreach($services as $service): 
                    ?>
                        <div class="col span_1_of_3">
                            <a href="?page=services&id=<?php echo $service->getId(); ?>"> 
                                <div class="miniature">
                                    <h3 class="description"> <?= $service->getName(); ?> <br> Szczegóły >></h3>
                                </div>  
                                
                            </a>
                        </div>

                        <?php if($i % 3 == 2 && $i < count($services) - 1): ?>
                            </div>
                            <div class="section group">
                        <?php endif; ?>
                    <?php 
                    $i = $i + 1; 
                    endforeach; 
                    ?>
                </div> 
                    
                <?php else: ?>
                    
                    <?php include(dirname(__DIR__).'/Servicescontroller/details.php'); ?>    

                <?php endif ?>
            </div>
        
    </div>                
</div>
</body>
</html>